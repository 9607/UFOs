%% TWO CLUSTERS ON TWO LEVELS (transient VHFOs, 1400Hz)
% SETTING
clear
rng(3) % seed
Path = [pwd, '\IN_sims\'];
clrs = ["b","r","g","k","c","y"];   % colors
per_clr = [0.9290,0.6940,0.1250];   % periodogram color

% TIME
T = 700;       % time span
dt = 0.01;

% COUPLING
eps1 = 0.05;    % within a cluster
eps2 = 0.01;    % between clusters
epsilon = [ eps1,eps2; ...
            eps2,eps1 ];
k = 2;              % number of clusters
n = 2*ones(1,k);    % number of neurons by cluster
N = sum(n);
K = getK(n,epsilon); % all-to-all coupling
K = K/N;

% MODEL PARAMETERS
gNa_0 = 30;
gK_0 = 20;
gL_0 = 0.1;
ENa_0 = 45;
EK_0 = -80;
EL_0 = -60;
I_0 = 24;

C_0 = [0.998,0.999,1,1.001];
% sigma_C = 0.001;
% C_0 = [ randtn(0.999,sigma_C,3*sigma_C,n(1)); 
%         randtn(1.001,sigma_C,3*sigma_C,n(2)) ];

% INITIAL CONDITIONS
nn = repelem(n/2,n);
nn = repmat(nn,[1,3]); % 3 corresponds to the number of equations
x_0 = repelem([ 40,-40,40,-40, ...
                0.25,0.25,0.27,0.27, ...
                0.5,0.5,0.52,0.52], nn);

% SIMULATION PARAMETERS
NN = 1;             % distinc parameter noise between neurons (N) or not (1)
shift = 0;          % transient period
sigma_noise = 1;    % std of the input current noise
sigma_par = 0;      % std of parameters' multiplicative noise N(0,sigma_par^2)

params = struct('gNa',gNa_0,'gK',gK_0,'gL',gL_0,'ENa',ENa_0, ...
    'EK',EK_0,'EL',EL_0,'I',I_0,'C',C_0);
data = struct('model','IN','params',params,'T',T,'dt',dt,'shift',shift,'n',n,'N',N,'K',K, ...
    'epsilon',epsilon,'sigma_par',sigma_par,'sigma_noise',sigma_noise,'heter',(N==NN));

% SIMULATION
NN = data.heter*data.N+(1-data.heter)*1; % distinct parameter noise between neurons or not
if data.sigma_par ~= 0
    fn = fieldnames(data.params);
    fn = setdiff(fn,{'C'}); % parameter C will not be random
    for k = 1:numel(fn)
        data.params = setfield(data.params,fn{k}, ...
            getfield(data.params,fn{k})*(1+data.sigma_par*randn(NN,1)));
    end
end

[t,x,ie] = simInterneuron(K,data.params,x_0,data.T,data.dt,data.shift,data.sigma_noise);
if ~isempty(ie), error('The orbit diverged!'), end
data.x = x;

% file = 'data_1400Hz.mat';
% for i = 1:length(dir(Path))-2, if isfile([Path,file]), file = ['data_',num2str(i),'.mat']; else, break; end, end
% save([Path,file],'data');

% PLOTTING
% signals and periodograms
int_selected = [575,600];       % selected interval
idx = (t >= int_selected(1)) & (t <= int_selected(2));
ind = 1+[0,cumsum(n)];          % indices for cluster distinction
x_sel = x(idx,ind(1:end-1));    % the first cell of each (synchronized) cluster

[P_smoothed,f] = periodogram(detrend(x_sel),[],2^18,1000/dt);
[P_i_max,P_i_max_ind] = max(P_smoothed);
[~,P_max_ind] = max(P_i_max);
ind_max = P_i_max_ind(P_max_ind);
P_sel_smoothed = P_smoothed(:,P_max_ind);
P_sel_max = P_sel_smoothed(ind_max); % maximum of P to be plotted
f_sel_max = f(ind_max); % frequency of the maximum of P to be plotted

% frequency of the composed signal
composed_signal = sum(x(:,1:N),2);
[P_comp_smoothed,f_comp] = periodogram(detrend(composed_signal(idx)),[],2^18,1000/dt);
[P_comp_max,P_comp_max_ind] = max(P_comp_smoothed); % maximum of P to be plotted
f_comp_max = f_comp(P_comp_max_ind);

[f_sel_max, f_comp_max, f_comp_max/f_sel_max]

% plotting
figure, tiledlayout(4,2,'TileSpacing','Compact','Padding','Compact')
f_max_tick = 1.8*1000; x_ticks = 0:0.2*1000:f_max_tick;
lwd = 0.2;
ind = 1+[0,cumsum(n)]; % indices for cluster distinction
N_pgrams = 0; % number of clusters periodograms to be plotted
nexttile([1,2]), hold on, for i = 1:k, plot(t,x(:,ind(i):ind(i+1)-1),'color',clrs(i),'LineWidth',lwd); end, hold off
xlabel('t [ms]'), ylabel('V_i [mV]'), xlim([200,700]), ylim([-50,40]), xticks(200:50:700), set(gca,'box','off')

nexttile([1,2]), plot(t,composed_signal,'b','LineWidth',lwd)
xlabel('t [ms]'), ylabel('V [mV]'), xlim([200,700]), ylim([-170,100]), xticks(200:50:700), set(gca,'box','off')

nexttile, hold on, for i = 1:k, plot(t,x(:,ind(i):ind(i+1)-1),'color',clrs(i),'LineWidth',lwd); end, hold off
xlabel('t [ms]'), ylabel('V_i [mV]'), xlim(int_selected), ylim([-50,40]), set(gca,'box','off')

nexttile, hold on, for i = 1:N_pgrams, plot(f,P_smoothed(:,i),'color',clrs(i),'LineWidth',lwd), end
plot(f,P_sel_smoothed,'color',per_clr), xlim([0,f_max_tick]), xticks(x_ticks), ylim([0,20]), yticks(0:10:20)
% plot(f_sel_max,P_sel_max,'*','MarkerSize',5,'Color','r')
xlabel('f [Hz]'), ylabel('|P_1(f)|'), set(gca,'box','off'), hold off

nexttile, plot(t,composed_signal,'b')
xlabel('t [ms]'), ylabel('V [mV]'), xlim(int_selected), ylim([-95,-15]), yticks(-100:20:-20), set(gca,'box','off')

nexttile, hold on, plot(f_comp,P_comp_smoothed,'color',per_clr)
% plot(f_comp_max,P_comp_max,'*','MarkerSize',5,'Color','r')
xlim([0,f_max_tick]), xticks(x_ticks), ylim([0,2]), yticks(0:1:2)
xlabel('f [Hz]'), ylabel('|P(f)|'), set(gca,'box','off'), hold off

