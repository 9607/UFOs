%% STABLE ANTI-PHASE CYCLE IN THE PARAMETER SPACE (C1,C2) or (C1,epsilon)
%% SETTING
clear
Path = [pwd, '\IN_sims\'];
rng(13) % seed

% TIME
T = 1500;   % time span
dt = 0.01;
N_eq = 3;   % number of equations

% PARAMETERS
gNa_0 = 30;
gK_0 = 20;
gL_0 = 0.1;
ENa_0 = 45;
EK_0 = -80;
EL_0 = -60;
I_0 = 24;
C_0 = 1;

% COUPLING
N = 2;                  % number of neurons
epsilon = 0.08;         % for C1 vs. C2 (for C1 vs. epsilon, this value may be arbitrary except 0)
K = getK(N,epsilon);    % all-to-all coupling
K = K/N;

% SIMULATION PARAMETERS
shift = 500;        % transient period
sigma_noise = 0;    % std of the input current noise
sigma_par = 0;      % std of parameters' multiplicative noise N(0,sigma_par^2)

% we will compare the frequencies of a selected oscillator (osc_ind) and composed signal
% osc_ind is taken as the oscillator with the highest natural frequency (choosen in the for cycle)
grid_par_1 = 'C'; % grid parameter: 'C' or 'epsilon'
grid_par_2 = 'C';
% grid_par_2 = 'epsilon';
N_vals_1 = 401; % number of considered values for the first grid parameter
N_vals_2 = 401;
grid_vals_1 = linspace(0.92,1.08,N_vals_1);
grid_vals_2 = linspace(0.92,1.08,N_vals_2);
% grid_vals_2 = linspace(0,0.12,N_vals_2);
N_sims = 1;     % number of simulations for each combination of grid_par
freq_ratios = nan(N_vals_1,N_vals_2,N_sims);    % ratios between the selected osc. freq. and the composed signal freq.
freq = nan(N_vals_1,N_vals_2,N_sims);           % frequencies of the selected oscillator (no. osc_ind)
freq_comp = nan(N_vals_1,N_vals_2,N_sims);      % frequencies of the composed signal
x0 = nan(N_eq*N,N_vals_1,N_vals_2,N_sims);      % initial conditions

NN = 1; % 1 for homogeneous oscillators, N for heterogeneous
threshold = 0.5; % threshold for the detection of higher oscillations (0.5 kHz)

%% SIMULATION ON A GRID
params = struct('gNa',gNa_0,'gK',gK_0,'gL',gL_0,'ENa',ENa_0, ...
    'EK',EK_0,'EL',EL_0,'I',I_0,'C',C_0);
data = struct('model','IN','params',params,'T',T,'dt',dt,'shift',shift, ...
    'N',N,'K',K,'epsilon',epsilon, ...
    'grid_par_1',grid_par_1, ...
    'grid_par_2',grid_par_2, ...
    'grid_vals_1',grid_vals_1, ...
    'grid_vals_2',grid_vals_2, ...
    'sigma_par',sigma_par, ...
    'sigma_noise',sigma_noise, ...
    'heter',(NN==N), ...
    'threshold',threshold);

% filling the grid (fixed initial conditions)
parfor i = 1:N_vals_1
    for j = 1:N_vals_2
        for k = 1:N_sims
            % initial conditions
            x_0 = [-40, 40, 0.25, 0.25, 0.5, 0.5]; % starting point (close to APS)

            % simulation
            [x0(:,i,j,k),f_max,f_comp_max] = simGridNode(i,j,K,data,x_0);

            freq(i,j,k) = f_max;
            freq_comp(i,j,k) = f_comp_max;
            freq_ratios(i,j,k) = f_comp_max/f_max;
        end
    end
    % sprintf('done: %.2f %%', 100*i/N_vals_1)
end

% testing the presence of higher frequency
tested = zeros(N_vals_1,N_vals_2,N_sims);   % indication whether the cell (i,j,k) has already been tested
is_red = freq_comp > threshold;
is_red_old = [];

while ~isequal(is_red,is_red_old)
    is_red_old = is_red;
    for i = 1:N_vals_1
        for j = 1:N_vals_2
            for k = 1:N_sims
                if is_red(i,j,k) && ~tested(i,j,k)
                    nbors = getNeighbors(i,j,N_vals_1,N_vals_2);
                    for m = 1:size(nbors,2)
                        ii = nbors(1,m);
                        jj = nbors(2,m);
                        if ~is_red(ii,jj,k)
                            x_0 = x0(:,i,j,k); % initial condidions determined by the red neighbor
                            [x0(:,ii,jj,k),f_max,f_comp_max] = simGridNode(ii,jj,K,data,x_0);

                            freq(ii,jj,k) = f_max;
                            freq_comp(ii,jj,k) = f_comp_max;
                            freq_ratios(ii,jj,k) = f_comp_max/f_max;
                            is_red(ii,jj,k) = f_comp_max > threshold;
                        end
                    end
                    tested(i,j,k) = 1;
                end
            end
        end
        % sprintf('done: %.2f %%', 100*i/N_vals_1)
    end
end

% testing the presence of NaN
tested = zeros(N_vals_1,N_vals_2,N_sims);
is_nan = isnan(freq_comp);
is_nan_old = [];

while ~isequal(is_nan,is_nan_old)
    is_nan_old = is_nan;
    for i = 1:N_vals_1
        for j = 1:N_vals_2
            for k = 1:N_sims
                if is_nan(i,j,k) && ~tested(i,j,k)
                    nbors = getNeighbors(i,j,N_vals_1,N_vals_2);
                    for m = 1:size(nbors,2)
                        ii = nbors(1,m);
                        jj = nbors(2,m);
                        if ~is_nan(ii,jj,k)
                            x_0 = x0(:,ii,jj,k); % initial condidions determined by the red neighbor
                            [x0(:,i,j,k),f_max,f_comp_max] = simGridNode(i,j,K,data,x_0);

                            freq(i,j,k) = f_max;
                            freq_comp(i,j,k) = f_comp_max;
                            freq_ratios(i,j,k) = f_comp_max/f_max;
                            is_nan(i,j,k) = isnan(f_comp_max);
                            if ~isnan(f_comp_max), tested(i,j,k) = 1; break; end
                        end
                    end
                    tested(i,j,k) = 1;
                end
            end
        end
        % sprintf('done: %.2f %%', 100*i/N_vals_1))
    end
end

data.x_0 = x0;
data.freq = freq;
data.freq_comp = freq_comp;
data.freq_ratios = freq_ratios;

% file = 'data.mat';
% for i = 1:length(dir(Path))-2, if isfile([Path,file]), file = ['data_',num2str(i),'.mat']; else, break; end, end
% save([Path,file],'data');

%% SIMULATION VISUALIZATION
M = 1000*data.freq_comp;
grid_vals_1 = data.grid_vals_1; 
grid_vals_2 = data.grid_vals_2;

M = rot90(M,1);
figure,imagesc(M,'AlphaData',~isnan(M))   
colormap(flip(autumn)), a = colorbar('Ticks',1000*(0.3:0.1:0.8)); caxis(1000*[0.3 0.8])
a.Label.String = 'Frequency [Hz]';
y_ticks_length = min(length(grid_vals_2),9);
yticklabs = round(linspace(max(grid_vals_2),min(grid_vals_2),y_ticks_length),4);
y_ticks = linspace(1, length(grid_vals_2), numel(yticklabs));
x_ticks_length = min(length(grid_vals_1),9); 
xticklabs = round(linspace(min(grid_vals_1),max(grid_vals_1),x_ticks_length),4);
x_ticks = linspace(1, length(grid_vals_1), numel(xticklabs));
set(gca,'DataAspectRatio',[1,1,1]), set(gca, 'XTick', x_ticks, 'XTickLabel', xticklabs), 
set(gca, 'YTick', y_ticks, 'YTickLabel', yticklabs)
if strcmp(data.grid_par_1,'C'), xlabel('C_1'), else, xlabel('$\varepsilon$','Interpreter','latex'), end 
if strcmp(data.grid_par_2,'C'), ylabel('C_2'), else, ylabel('$\varepsilon$','Interpreter','latex'), end
xtickangle(0)
