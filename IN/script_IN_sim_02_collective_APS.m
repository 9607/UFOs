%% TRANSIENT COLLECTIVE ANTI-PHASE BEHAVIOR (HFOs, 610 Hz)
% SETTING
clear
rng(19) % seed
Path = [pwd, '\IN_sims\'];
clrs = ["b","r","g","k","c","y"];   % colors
per_clr = [0.9290,0.6940,0.1250];   % periodogram color

% TIME
T = 800;    % time span
dt = 0.01;

% COUPLING
eps1 = 0.05;
eps2 = 0.01;
epsilon = [ eps1, eps2; ...
            eps2, eps1 ];
n = [25, 25];   % number of neurons by cluster
k = length(n);  % number of clusters
N = sum(n);
K = getK(n,epsilon);
K = K/N;

% MODEL PARAMETERS
gNa_0 = 30;
gK_0 = 20;
gL_0 = 0.1;
ENa_0 = 45;
EK_0 = -80;
EL_0 = -60;
I_0 = 20;

sigma_C = 0.03;
C_0 = [ randtn(1,sigma_C,3*sigma_C,n(1)); 
        randtn(1,sigma_C,3*sigma_C,n(2)) ];

% INITIAL CONDITIONS
x_0 = repelem([-40, 40, 0.25, 0.25, 0.5, 0.5],N/2);

% SIMULATION PARAMETERS
NN = 1;             % distinc parameter noise between neurons (N) or not (1)
shift = 0;          % transient period
sigma_noise = 1;    % std of the input current noise
sigma_par = 0;      % std of parameters' multiplicative noise N(0,sigma_par^2)

params = struct('gNa',gNa_0,'gK',gK_0,'gL',gL_0,'ENa',ENa_0, ...
    'EK',EK_0,'EL',EL_0,'I',I_0,'C',C_0);
data = struct('model','IN','params',params,'T',T,'dt',dt,'shift',shift,...
    'n',n,'K',K,'epsilon',epsilon,'sigma_par',sigma_par,...
    'sigma_noise',sigma_noise,'heter',(NN==N),'x_0',x_0);

% SIMULATION
if data.sigma_par ~= 0
    fn = fieldnames(data.params);
    fn = setdiff(fn,{'C'}); % C is not supposed to be random
    for k = 1:numel(fn)
        data.params = setfield(data.params,fn{k},getfield(data.params,fn{k})*(1+data.sigma_par*randn(NN,1)));
    end
end

[t,x,ie] = simInterneuron(K,data.params,x_0,data.T,data.dt,data.shift,data.sigma_noise);
if ~isempty(ie), error('The orbit diverged!'), end
data.x = x;

% file = 'data.mat';
% for i = 1:length(dir(Path))-2, if isfile([Path,file]), file = ['data_',num2str(i),'.mat']; else, break; end, end
% save([Path,file],'data');

% PLOTTING
% signals and periodograms
ind = 1+[0,cumsum(n)];          % indices for cluster distinction
int_selected = [415,440];       % selected interval
idx = (t >= int_selected(1)) & (t <= int_selected(2));
x_sel = x(idx,ind(1:end-1));   % the first cell of each (synchronized) cluster

[P_smoothed,f] = periodogram(detrend(x_sel),[],2^18,1000/dt);
[P_i_max,P_i_max_ind] = max(P_smoothed);
[~,P_max_ind] = max(P_i_max);
ind_max = P_i_max_ind(P_max_ind);
P_sel_smoothed = P_smoothed(:,P_max_ind);
P_sel_max = P_sel_smoothed(ind_max); % maximum of P to be plotted
f_sel_max = f(ind_max); % frequency of the maximum of P to be plotted

% frequency of the composed signal
composed_signal = sum(x(:,1:N),2);
[P_comp_smoothed,f_comp] = periodogram(detrend(composed_signal(idx)),[],2^18,1000/dt);
[P_comp_max,P_comp_max_ind] = max(P_comp_smoothed);
f_comp_max = f_comp(P_comp_max_ind);

[f_sel_max, f_comp_max, f_comp_max/f_sel_max]

% plotting
figure, tiledlayout(4,2,'TileSpacing','Compact','Padding','Compact')
f_max_tick = 1.4*1000; x_ticks = 0:0.2*1000:f_max_tick;
lwd = 0.2;
N_pgrams = 0; % number of periodograms to be plotted

nexttile([1,2]), hold on, for i = 1:k, plot(t,x(:,ind(i):ind(i+1)-1),'color',clrs(i),'LineWidth',lwd); end, hold off, set(gca,'box','off')
xlabel('t [ms]'), ylabel('V_i [mV]'), xlim([0,500]), ylim([-50,40]), xticks(0:50:500)

nexttile([1,2]), plot(t,composed_signal,'b','LineWidth',lwd)
xlabel('t [ms]'), ylabel('V [mV]'), xlim([0,500]), ylim([-1600,0]), xticks(0:50:500), set(gca,'box','off')

nexttile, hold on, for i = 1:k, plot(t(idx),x(idx,ind(i):ind(i+1)-1),'color',clrs(i),'LineWidth',lwd); end, hold off
xlabel('t [ms]'), ylabel('V_i [mV]'), xlim(int_selected), ylim([-50,40]), set(gca,'box','off')

nexttile, hold on, for i = 1:N_pgrams, plot(f,P_smoothed(:,i),'color',clrs(i),'LineWidth',lwd), end
plot(f,P_sel_smoothed,'color',per_clr), xlim([0,f_max_tick]), xticks(x_ticks), ylim([0,20])
% plot(f_sel_max,P_sel_max,'*','MarkerSize',5,'Color','r')
xlabel('f [Hz]'), ylabel('|P_1(f)|'), set(gca,'box','off'), hold off

nexttile, plot(t(idx),composed_signal(idx),'b'), set(gca,'box','off')
xlabel('t [ms]'), ylabel('V [mV]'), xlim(int_selected), ylim([-1000,-400]), yticks(-1000:200:-400)

nexttile, hold on, plot(f_comp,P_comp_smoothed,'color',per_clr), xlim([0,f_max_tick]), xticks(x_ticks)
% plot(f_comp_max,P_comp_max,'*','MarkerSize',5,'Color','r')
xlabel('f [Hz]'), ylabel('|P(f)|'), set(gca,'box','off'), hold off

