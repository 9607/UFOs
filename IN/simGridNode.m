function [new_x_0,f_max,f_comp_max] = simGridNode(i,j,K,data,x_0)
%SIMGRIDNODE i, j, K, data, x_0
%   Simulates one step in a grid simulation.
%   i,j     node indices
%   K       coupling matrix
%   data    data structure
%   x_0     initial conditions

if isfield(data,'grid_par')
    data.grid_par_1 = data.grid_par;
    data.grid_par_2 = data.grid_par;
end

% parameters (possibly random)
NN = data.heter*data.N+(1-data.heter)*1;
if data.sigma_par ~= 0
    fn = fieldnames(data.params);
    for k = 1:numel(fn)
        data.params = setfield(data.params,fn{k},getfield(data.params,fn{k})*(1+data.sigma_par*randn(NN,1)));
    end
end

if strcmp(data.grid_par_1,'C')
    if strcmp(data.grid_par_2,'C') % C1 and C2
        data.params.C = [data.grid_vals_1(i),data.grid_vals_2(j)];
    else % C1 and epsilon
        data.params.C = [data.grid_vals_1(i),data.params.C];
        K = (K>0)*data.grid_vals_2(j);
    end
else % epsilon and C1
    K = (K>0)*data.grid_vals_1(i);
    data.params.C = [data.grid_vals_2(j),data.params.C];
end

[~,osc_ind] = min(data.params.C); % osc with the highest frequency

% simulation
[~,x,ie] = simInterneuron(K,data.params,x_0,data.T,data.dt,data.shift,data.sigma_noise);
if (~isempty(ie)), new_x_0 = x_0; f_max = nan(); f_comp_max = nan(); return; end % the orbit diverged

% frequency of the selected oscillator (no. osc_ind)
f_max = fmax(x(:,osc_ind),data.dt);

% frequency of the composed signal
composed_signal = sum(x(:,1:end/2),2);
f_comp_max = fmax(composed_signal,data.dt);

ten_periods = 10*min(4,1/f_max); % approximation of the maximum period = 4 [ms] 
last_10_periods_start = size(x,1)-floor(ten_periods/data.dt);
if last_10_periods_start < 1
    new_x_0 = x_0; f_max = nan(); f_comp_max = nan(); % the orbit diverged
else
    [~,ind_max] = max(x(last_10_periods_start-100:end-100,1)); % maximum of the first variable (in the last 10 periods)
    ind_max = last_10_periods_start + ind_max; % index of the found maximum
    if size(x,1) < ind_max 
        new_x_0 = x_0; % something went wrong
    else
        new_x_0 = x(ind_max,:); % initial condition for a neighbor
    end
end   
end
