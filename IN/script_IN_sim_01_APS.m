%% ANTI-PHASE SYNCHRONY (HFOs, 745 Hz)
clear
rng(17) % seed
Path = [pwd, '\IN_sims\'];
clrs = ["b","r","g","k","c","y"];   % colors
per_clr = [0.9290,0.6940,0.1250];   % periodogram color

% TIME
T = 3000;       % time span
dt = 0.01;

% COUPLING
epsilon = 0.08;
n = 2;
N = sum(n);
K = getK(n,epsilon);
K = K/N;

% MODEL PARAMETERS
gNa_0 = 30;
gK_0 = 20;
gL_0 = 0.1;
ENa_0 = 45;
EK_0 = -80;
EL_0 = -60;
I_0 = 24;
C_0 = [0.99, 1];

% INITIAL CONDITIONS
x_0 = [0,-10,0.3,0.05,0.42,0.55];

% SIMULATION PARAMETERS
NN = 1;             % distinc parameter noise between neurons (N) or not (1)
shift = 200;        % transient period
sigma_noise = 0;    % std of the input current noise
sigma_par = 0;      % std of parameters' multiplicative noise N(0,sigma_par^2)

params = struct('gNa',gNa_0,'gK',gK_0,'gL',gL_0,'ENa',ENa_0, ...
    'EK',EK_0,'EL',EL_0,'I',I_0,'C',C_0);
data = struct('model','IN','params',params,'T',T,'dt',dt,'shift',shift,...
    'N',N,'K',K,'epsilon',epsilon,'sigma_par',sigma_par,...
    'sigma_noise',sigma_noise,'heter',(NN==N),'x_0',x_0);

% SIMULATION
if data.sigma_par ~= 0
    fn = fieldnames(data.params);
    fn = setdiff(fn,{'C'}); % C is not supposed to be random
    for k = 1:numel(fn)
        data.params = setfield(data.params,fn{k},getfield(data.params,fn{k})*(1+data.sigma_par*randn(NN,1)));
    end
end

[t,x,ie] = simInterneuron(K,data.params,x_0,data.T,data.dt,data.shift,data.sigma_noise);
if ~isempty(ie), error('The orbit diverged!'), end
data.x = x;

% file = 'data.mat';
% for i = 1:length(dir(Path))-2, if isfile([Path,file]), file = ['data_',num2str(i),'.mat']; else, break; end, end
% save([Path,file],'data');

% PLOTTING
% signals and periodograms
int_selected = [400,500];   % selected interval
idx = (t >= int_selected(1)) & (t <= int_selected(2));

% frequency of the selected oscillator
[P_smoothed,f] = periodogram(detrend(x(idx,1:N)),[],2^18,1000/dt);
[P_i_max,P_i_max_ind] = max(P_smoothed);
[~,P_max_ind] = max(P_i_max);
ind_max = P_i_max_ind(P_max_ind);
P_sel_smoothed = P_smoothed(:,P_max_ind);
P_sel_max = P_sel_smoothed(ind_max); % maximum of P to be plotted
f_sel_max = f(ind_max); % frequency of the maximum of P to be plotted

% frequency of the composed signal
composed_signal = sum(x(:,1:N),2);
[P_comp_smoothed,f_comp] = periodogram(detrend(composed_signal(idx)),[],2^18,1000/dt);
[P_comp_max,P_comp_max_ind] = max(P_comp_smoothed);
f_comp_max = f_comp(P_comp_max_ind);

[f_sel_max, f_comp_max, f_comp_max/f_sel_max]

% plotting
figure, tlo = tiledlayout(3,2,'TileSpacing','Compact');
f_max_tick = 1.4*1000; x_ticks = 0:0.2*1000:f_max_tick;
N_pgrams = 0;   % number of clusters periodograms to be plotted

nexttile([1,2]), hold on, for i = 1:N, plot(t,x(:,i),'color',clrs(i)), end, set(gca,'box','off')
xlabel('t [ms]'), ylabel('V_i [mV]'), xlim([0,500]), ylim([-50,40]), xticks(0:50:500)

nexttile, hold on, for i = 1:N, plot(t(idx),x(idx,i),'color',clrs(i)), end, set(gca,'box','off')
xlabel('t [ms]'), ylabel('V_i [mV]'), xlim(int_selected), ylim([-45,35])

nexttile, hold on, for i = 1:N_pgrams, plot(f,P(:,i),'color',clrs(i)), end
plot(f,P_sel_smoothed,'color',per_clr), xlim([0,f_max_tick]), xticks(x_ticks)
% plot(f_sel_max,P_sel_max,'*','MarkerSize',5,'Color','r')
xlabel('f [Hz]'), ylabel('|P(f)|'), set(gca,'box','off'), hold off

nexttile, plot(t(idx),composed_signal(idx),'b'), set(gca,'box','off')
xlabel('t [ms]'), ylabel('V [mV]'), xlim(int_selected), ylim([-65,10])

nexttile, hold on, plot(f_comp,P_comp_smoothed,'color',per_clr), 
xlim([0,f_max_tick]), xticks(x_ticks)
% plot(f_comp_max,P_comp_max,'*','MarkerSize',5,'Color','r')
xlabel('f [Hz]'), ylabel('|P(f)|'), set(gca,'box','off'), hold off

% phase space (V1,V2)
figure,plot(x(:,1),x(:,2),'b','LineWidth',0.2)
xlabel('V_1 [mV]'),ylabel('V_2 [mV]'),xlim([-40,30]),ylim([-40,30])
set(gca,'DataAspectRatio',[1,1,1],'XTick',-40:10:30,'YTick',-40:10:30,'box','off')