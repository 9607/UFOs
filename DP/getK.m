function K = getK(n,epsilon)
%GETK n, epsilon
%   Returns a coupling matrix with given characteristics.
%   n           number of vertices within each group
%   epsilon     coupling between groups of vertices

k = length(n);      % number of groups
N = sum(n);         % number of vertices
s = size(epsilon);

if s(1) ~= s(2), error('Input argument epsilon must be a square matrix!'), end
if max(s) ~= k, error('Incorrect dimensions of n and epsilon!'), end

K = zeros(N);
ind = 1+[0,cumsum(n)]; % indices of the first nodes in each group

for i = 1:s(1)
    for j = 1:s(2)
        K(ind(i):ind(i+1)-1, ind(j):ind(j+1)-1) = ones(n(i),n(j))*epsilon(i,j);
    end
end

K = K-diag(diag(K));
end

