function x_dot = dp(x,K,gNa,gK,gL,gKm,C,ENa,EK,EL,Vt,Vs,I)
    % SOURCE: https://journals.physiology.org/doi/full/10.1152/jn.1999.81.4.1531  
    N = length(x)/5;
    
    V = x(1:N);
    m = x(N+1:2*N);
    h = x(2*N+1:3*N);
    n = x(3*N+1:4*N);
    mK = x(4*N+1:5*N);
    
    % sodium
    am = -.32*(V-Vt-13) ./ (exp(-(V-Vt-13)/4)-1);
    bm = .28*(V-Vt-40) ./ (exp((V-Vt-40)/5)-1);
    ah = .128*exp(-(V-Vt-Vs-17)/18);
    bh = 4 ./ (1+exp(-(V-Vt-Vs-40)/5));
    INa = gNa.*m.^3.*h.*(V-ENa);
    
    % delayed rectifier
    an = -.032*(V-Vt-15) ./ (exp(-(V-Vt-15)/5)-1);
    bn = .5*exp(-(V-Vt-10)/40);
    IKdr = gK.*n.^4.*(V-EK);
    
    % slow potassium M-current
    aKM = .0001*(V+30) ./ (1-exp(-(V+30)/9));
    bKM = -.0001*(V+30) ./ (1-exp((V+30)/9));
    IKM = gKm.*mK.*(V-EK);
    
    x_dot = [(I-gL.*(V-EL)-IKdr-INa-IKM+sum(K.*(V'-V),2)) ./ C;
        am.*(1-m)-bm.*m;
        ah.*(1-h)-bh.*h;
        an.*(1-n)-bn.*n;
        aKM.*(1-mK)-bKM.*mK];
end