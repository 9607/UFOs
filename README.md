# UFOs
Development and Support for modeling VHFOs and UFOs in EEG related to focal epilepsy.

This repository contains the necessary code for the execution of simulations that are included in the paper Weak coupling of neurons enables very high-frequency and ultra-fast oscillations through the interplay of synchronized phase-shifts. 

These subfolders include the following scripts and functions:
* DP (Destexhe–Paré model)
    - script_DP_sim_01_collective_APS.m – transient collective anti-phase behavior in an all-to-all coupled neuronal network composed of 50 DP neurons (Fig 13);
* IN (interneuron model)
    - script_IN_sim_01_APS.m – quasi-periodic anti-phase behavior of two coupled IN neurons (Fig 16);
    - script_IN_sim_02_collective_APS.m – transient collective anti-phase behavior in an all-to-all coupled neuronal network composed of 50 IN neurons (Fig 8);
    - script_IN_sim_03_grid.m – grid simulation of a double dominant frequency in the summed composed signal of two coupled IN neurons (Fig 7);
    - script_IN_sim_04_1400Hz.m – simulation of a 4-fold natural frequency in the summed composed signal of four coupled IN neurons (Fig 10);
    - script_IN_sim_05_2000Hz.m – simulation of a 6-fold natural frequency in the summed composed signal of six coupled IN neurons (Fig 12);
    - CoupledIN.txt – MatCont input used for bifurcation analysis of two coupled neurons, with parameters and equations (Fig 7). Can be adapted to 1 neuron.
* ML (Morris–Lecar model)
    - script_ML_sim_01_IPS_APS.m – in-phase and anti-phase synchrony of two ML neurons (Fig 14, 15);
    - script_ML_sim_02_collective_APS.m – collective anti-phase behavior in an all-to-all coupled neuronal network composed of 50 ML neurons (Fig 5);
    - script_ML_sim_03_collective_APS_transient – transient collective anti-phase behavior as a result of external input to an all-to-all coupled neuronal network composed of 50 ML neurons (Fig 6);
    - script_ML_sim_04_grid.m – grid simulation of a double dominant frequency in the summed composed signal of two coupled ML neurons (Fig 4);
    - CoupledML.txt – MatCont input used for bifurcation analysis of two coupled neurons, with parameters and equations (Fig 3, 4). Can be adapted to 1 neuron (Fig 1).
* Other scripts/routines used for pre/postprocessing and running the simulations
    - dp.m, interneuron.m, ml.m – functions determining the right-hand side of dynamical systems of coupled DP, IN, and ML neurons;
    - getK.m – creates gap-junctional coupling matrix;
    - em.m – integration routine for Euler–Maruyama;
    - fmax.m – computes the dominant frequency for a signal (Fig 4, 7);
    - getNeighbors.m – provides indices used to find anti-phase solutions in a grid search (Fig 4, 7);
    - ml_EM.m – integration routine for a simulation of external input resulting in a transient collective anti-phase behavior (Fig 6);
    - randtn.m – returns a truncated normal distribution for given parameters following Matlab documentation;
    - simDP, simInterneuron, simML – simulates a corresponding neuronal network of coupled DP, IN, and ML neurons;
    - simGridNode.m – refines the result of the grid search (Fig 4, 7), applied repeatedly till convergence.

Software versions used: MATLAB2021a and MatCont7p3

Copyright (c) 2023 Authors
