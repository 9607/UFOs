function [f_max] = fmax(x,dt)
%FMAX x, dt
%   Returns the estimation of maximal frequency [kHz] in the given signal x
%   x       signal
%   dt      time step size [ms]

[P_smoothed,f] = periodogram(detrend(x),[],2^18,1/dt);
[~,P_max_ind] = max(P_smoothed);
f_max = f(P_max_ind);
end

