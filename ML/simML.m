function [t,x,ie] = simML(K,params,x_0,T,dt,shift,sigma_noise)
%SIMML K, params, x_0, T, dt, shift, sigma_noise
%   Simulates coupled populations of N Morris-Lecar neurons.
%   K           coupling matrix (N x N non-negative matrix)
%   params      model parameters (structure)
%   x_0         initial conditions (vector of length 2*N)
%   T           time (length of the time interval)
%   dt          time step size
%   shift       whole number between 0 and T (transient period)
%   sigma_noise std of a white noise added to the signal during the integration

if (shift < 0) || (shift > T)
    error('shift = %d must be between 0 and T = %d!',shift,T)
end

N_eq = length(K);
t = 0:dt:T;
g_Ca = params.g_Ca(:); g_K = params.g_K(:); g_L = params.g_L(:);
C = params.C(:); E_Ca = params.E_Ca(:); E_K = params.E_K(:); 
E_L = params.E_L(:); phi = params.phi(:); V1 = params.V1(:);
V2 = params.V2(:); V3 = params.V3(:); V4 = params.V4(:); I = params.I(:);

ie = [];
Opt = odeset('Events', @myEvent);
if sigma_noise == 0
    [t,x,~,~,ie] = ode45(@(t,x) ml(x,K,g_Ca,g_K,g_L,C,E_Ca,E_K,E_L,phi,V1,V2,V3,V4,I), t, x_0, Opt);
else
    % The scalar standard deviation of the noise is scaled by the vector C 
    % containing the capacitance of each neuron, turning this into a vector.
    [t,x] = em(@(t,x) ml(x,K,g_Ca,g_K,g_L,C,E_Ca,E_K,E_L,phi,V1,V2,V3,V4,I), sigma_noise ./ C, x_0, T, dt, N_eq);
end

x = x(t>shift,:);
t = t(t>shift);
if ~isempty(t), t = t-t(1); end
end

function [value, isterminal, direction] = myEvent(~,x)
value = (max(abs(x)) > 5000); % if x diverges
isterminal = 1; % halt the integration
direction  = 0; % in any direction
end
