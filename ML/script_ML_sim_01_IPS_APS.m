%% IN-PHASE AND ANTI-PHASE SYNCHRONY
clear
rng(17) % seed
Path = [pwd, '\ML_sims\'];
clrs = ["b","r","g","k","c","y"];   % colors
per_clr = [0.9290,0.6940,0.1250];   % periodogram color

% TIME
T = 1500;       % time span
dt = 0.01;

% COUPLING
epsilon = 0.10;
n = 2;
N = sum(n);
K = getK(n,epsilon); % all-to-all
K = K/N;

% MODEL PARAMETERS
g_Ca_0 = 4;
g_K_0 = 8;
g_L_0 = 2;
E_Ca_0 = 120;
E_K_0 = -80;
E_L_0 = -60;
phi_0 = 1/15;
V1_0 = -1.2;
V2_0 = 18;
V3_0 = 10;
V4_0 = 17.4;
I_0 = 43;
C_0 = [1.2,1];

% INITIAL CONDITIONS
x_0 = [40, 40, 0.04, 0.04];     % IPS
% x_0 = [40, -40, 0.04, 0.04];    % APS

% SIMULATION PARAMETERS
NN = 1;             % distinc parameter noise between neurons (N) or not (1)
shift = 0;          % transient period
sigma_noise = 0;    % std of the input current noise
sigma_par = 0;      % std of parameters' multiplicative noise N(0,sigma_par^2)

params = struct('g_Ca',g_Ca_0,'g_K',g_K_0,'g_L',g_L_0,'E_Ca',E_Ca_0, ...
    'E_K',E_K_0,'E_L',E_L_0,'phi',phi_0,'V1',V1_0,'V2',V2_0,'V3',V3_0, ...
    'V4',V4_0,'I',I_0,'C',C_0);
data = struct('model','ML','params',params,'T',T,'dt',dt,'shift',shift,...
    'N',N,'K',K,'epsilon',epsilon,'sigma_par',sigma_par,...
    'sigma_noise',sigma_noise,'heter',(NN==N),'x_0',x_0);

% SIMULATION
if data.sigma_par ~= 0
    fn = fieldnames(data.params);
    fn = setdiff(fn,{'C'}); % C is not supposed to be random
    for k = 1:numel(fn)
        data.params = setfield(data.params,fn{k},getfield(data.params,fn{k})*(1+data.sigma_par*randn(NN,1)));
    end
end

[t,x,ie] = simML(K,data.params,x_0,data.T,data.dt,data.shift,data.sigma_noise);
if ~isempty(ie), error('The orbit diverged!'), end
data.x = x;

% file = 'data.mat';
% for i = 1:length(dir(Path))-2, if isfile([Path,file]), file = ['data_',num2str(i),'.mat']; else, break; end, end
% save([Path,file],'data');

% PLOTTING
% signals and periodograms
int_selected = [700,1000];   % selected interval
idx = (t >= int_selected(1)) & (t <= int_selected(2));

% frequency of the selected oscillator
[P_smoothed,f] = periodogram(detrend(x(idx,1:N)),[],2^18,1000/dt);
[P_i_max,P_i_max_ind] = max(P_smoothed);
[~,P_max_ind] = max(P_i_max);
ind_max = P_i_max_ind(P_max_ind);
P_sel_smoothed = P_smoothed(:,P_max_ind);
P_sel_max = P_sel_smoothed(ind_max); % maximum of P to be plotted
f_sel_max = f(ind_max); % frequency of the maximum of P to be plotted

% frequency of the composed signal
composed_signal = sum(x(:,1:N),2);
[P_comp_smoothed,f_comp] = periodogram(detrend(composed_signal(idx)),[],2^18,1000/dt);
[P_comp_max,P_comp_max_ind] = max(P_comp_smoothed);
f_comp_max = f_comp(P_comp_max_ind);

[f_sel_max, f_comp_max, f_comp_max/f_sel_max]

% plotting
figure, tlo = tiledlayout(3,2,'TileSpacing','Compact','Padding','Compact');
f_max_tick = 0.3*1000; x_ticks = 0:0.05*1000:f_max_tick;
N_pgrams = 0;   % number of clusters periodograms to be plotted

nexttile([1,2]), hold on, for i = 1:N, plot(t,x(:,i),'color',clrs(i)), end, set(gca,'box','off')
xlabel('t [ms]'), ylabel('V_i [mV]'), xlim([0,1500]), ylim([-65,60]), xticks(0:100:1500)

nexttile, hold on, for i = 1:N, plot(t(idx),x(idx,i),'color',clrs(i)), end, set(gca,'box','off')
xlabel('t [ms]'), ylabel('V_i [mV]'), xlim(int_selected), ylim([-65,60])

nexttile, hold on, for i = 1:N_pgrams, plot(f,P(:,i),'color',clrs(i)), end
% plot(f_sel_max,P_sel_max,'*','MarkerSize',5,'Color','r')
plot(f,P_sel_smoothed,'color',per_clr), set(gca,'box','off'),hold off
xlim([0,f_max_tick]), xticks(x_ticks), xlabel('f [Hz]'), ylabel('|P(f)|')
ylim([0,150]), yticks(0:50:150) % IPS
% ylim([0,100]), yticks(0:50:100) % APS

nexttile, plot(t(idx),composed_signal(idx),'b'), set(gca,'box','off')
xlabel('t [ms]'), ylabel('V [mV]'), xlim(int_selected)
ylim([-125,110]) % IPS
% ylim([-100,20]) % APS

nexttile, hold on, plot(f_comp,P_comp_smoothed,'color',per_clr),
% plot(f_comp_max,P_comp_max,'*','MarkerSize',5,'Color','r')
xlim([0,f_max_tick]), xticks(x_ticks), xlabel('f [Hz]'), ylabel('|P(f)|')
set(gca,'box','off'), hold off
ylim([0,600]), yticks(0:200:600) % IPS
% ylim([0,400]), yticks(0:200:400) % APS

