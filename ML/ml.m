function x_dot = ml(x,K,g_Ca,g_K,g_L,C,E_Ca,E_K,E_L,phi,V1,V2,V3,V4,I)
    % SOURCE: https://doi.org/10.1007/978-3-662-61184-5
    N = length(x)/2;
    
    V = x(1:N);
    w = x(N+1:end);
    
    x_dot = [(-g_Ca * 1/2 .* (1 + tanh((V - V1) ./ V2)) .* (V - E_Ca) - ...
                    g_K .* w .* (V - E_K) - g_L .* (V - E_L) + I + ...
                    sum(K .* (V' - V), 2)) ./ C;
            phi .* (1/2 * (1 + tanh((V - V3) ./ V4)) - w) .* cosh((V - V3) ./ (2 * V4)) ];
end