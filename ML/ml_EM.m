function [t,x] = ml_EM(K,g_Ca,g_K,g_L,C,E_Ca,E_K,E_L,phi,V1,V2,V3,V4,I0,sigma_noise,x0,T,dt)
% SOURCE: https://doi.org/10.1007/978-3-662-61184-5
    N = length(K);
    t = 0:dt:T;
    V = x0(1:N);
    W = x0(N+1:end);
    x = nan(2*N,length(t));
    x(:,1) = x0;
    for ii=2:length(t)
       if (t(ii)>716 && t(ii)<718)
          I=I0+40*[ones(N/2,1);zeros(N/2,1)];
       else
          I=I0;
       end
       DV = dt*(-g_Ca*1/2*(1 + tanh((V - V1)/V2)).*(V - E_Ca) - ...
                    g_K* W .* (V - E_K) - g_L .* (V - E_L) + I + ...
                    sum(K .* (V' - V), 2) + sigma_noise/sqrt(dt)*randn(N,1)) ./ C;
       DW = dt*phi .* (1/2 * (1 + tanh((V - V3)/V4)) - W) .* cosh((V - V3)/(2*V4));
       V = V+DV; W = W+DW;
       x(:,ii) = [V;W];
    end
    
    x = x';
end