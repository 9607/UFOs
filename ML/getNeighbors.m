function [coords] = getNeighbors(i,j,N_vals_1,N_vals_2)
%GETNEIGHBORS i,j,N_vals_1,N_vals_2
%   Returns grid coordinates of neighbors for the given cell (i,j)

coords = [i-1,i,i+1,i,i-1,i-1,i+1,i+1; j,j+1,j,j-1,j-1,j+1,j+1,j-1];
% coords = [i-1,i,i+1,i; j,j+1,j,j-1];
coords = coords(:,coords(1,:)>=1 & coords(1,:)<=N_vals_1 & coords(2,:)>=1 & coords(2,:)<=N_vals_2);
end
